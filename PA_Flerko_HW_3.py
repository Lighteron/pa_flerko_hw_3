# 1. Выводит строку из length случайных символов англ алфавита.
# Принимает следующие необязательные аргументы:
# length. Регулирует длину случайной строки, значение по умолчанию: 10
# specials. Регулирет включение спец символов англ алфавита в сгенерированную строку: !"№;%:?*$()_+.
# Работает как булевый параметр: 1 -> True, 0 -> False. По умолчанию символы не включаются.
# digits. Регулирет включение цифр в сгенерированную строку: 0123456789.
# Работает как булевый параметр: 1 -> True, 0 -> False.
# По умолчанию цифры не включаются.
# Предусмотреть проверку всех аргументов:
# length: число, в диапазоне от 1 до 100.
# specials: 0 или 1
# digits: 0 или 1
# get_password() -> 127.0.0.1:5000/password?length=42
#
# 2. Создать view-функцию, которая выводит курс биткойна для заданной валюты (https://bitpay.com/api/rates ).
# Для этого установить и использовать пакет requests: https://pypi.org/project/requests/.
# Параметр currency необязательный, по умолчанию используется USD.
# def get_bitcoin_rate() -> /bitcoin_rate?currency=UAH

# 3*. [Необязательно] Вернуть результаты заданий 1 и 2 в виде html-страницы (пример в аттаче).
# Для этого разобраться с тем, что такое шаблоны и как они рендерятся: https://pythonru.com/uroki/6-shablony-vo-flask

import random
import string
from requests import get
from marshmallow import validate
from flask import Flask, render_template, Response
from webargs import fields
from webargs.flaskparser import use_kwargs

password_args = {
        "length": fields.Int(
            missing=10,
            validate=[validate.Range(min=1, max=100)],
        ),
        "specials": fields.Int(
            missing=0,
            validate=[validate.Range(min=0, max=1)],
        ),
        "digits": fields.Int(
            missing=0,
            validate=[validate.Range(min=0, max=1)],
        )
    }

app = Flask(__name__)


@app.errorhandler(400)
@app.errorhandler(404)
@app.errorhandler(422)
def handle_error(err):
    return Response("Error", status=err.code)


@app.route("/password")
@use_kwargs(password_args, location="query")
def get_password(length, specials, digits):
    characters = string.ascii_lowercase + string.ascii_uppercase
    if specials == 1:
        characters += string.punctuation
    if digits == 1:
        characters += string.digits
    password = "".join(
        random.choices(
            characters,
            k=length
        )
    )

    return render_template("password.html", title="Password Generator", password=password)


currency_args = {
        "currency": fields.Str(
            missing="USD",
            validate=lambda curr_code: len(curr_code) == 3
        )
}


@app.route("/bitcoin_rate")
@use_kwargs(currency_args, location="query")
def get_bitcoin_rate(currency):
    url = "https://bitpay.com/api/rates"
    print(currency)
    try:
        response = get(url).json()
    except:
        return "Error, something went wrong"

    for elem in response:
        if elem["code"] == currency:
            res = f"Price to buy bitcoin for {elem['name']} ({elem['code']}) - rate: {elem['rate']}"
            return render_template("bitcoin_currency_rate.html", title="BTC Rate", res=res)


app.run(debug=True)
